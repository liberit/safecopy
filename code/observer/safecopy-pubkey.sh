#!/bin/bash
#    SafeCopy is a set of scripts for doing pull safecopys from windows
#    Copyright (C) 2016  LiberIT Liberty Information Technology Services
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# analyze input
while [[ $# -gt 1 ]]
do
key="$1"

export PATH="$PATH:/usr/bin/"

case $key in
    -f|--from) #IP or resolvable hostname
    FROM="$2"
    shift # past argument
    ;;
    -t|--to) # destination directory
    TO="$2"
    shift # past argument
    ;;
    -d|--directories)
	echo "$2"
    eval "$2"
    shift # past argument
    ;;
    -n|--newspaper) #log
    NEWSPAPER_DIRECTORY="$2"
    shift # past argument
    ;;
    -s|--shutdown) #log
    SHUTDOWN="true $2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

# establish defaults

eval "$RSYNC_EVAL"
if [ -z "${RSYNC_OPTS}" ]; 
then 
  RSYNC_OPTS=(-rzas --delete --no-p --no-g --no-o --inplace -e "ssh -C")
fi
SUCCESS=0
if [ -z "${NEWSPAPER_DIRECTORY}" ]; 
then 
  NEWSPAPER_DIRECTORY="/tmp/"
fi
function rsync_folder () {
  #shellcheck disable=SC2068
	echo sudo rsync ${RSYNC_OPTS[@]} "$FROM":shadow/"$1" "$TO"  
RSYNC_OUT=$(sudo rsync ${RSYNC_OPTS[@]} "$FROM":shadow/"$1" "$TO" >> /tmp/rsync.log)
  RSYNC_EXIT=$?
	if [ "$RSYNC_EXIT" = "$SUCCESS" ] || [ "$RSYNC_EXIT" = 23 ]
	then
	  { date; echo "$FROM $1 safecopy success" ; 
    }  >> "${NEWSPAPER_DIRECTORY}/safecopy.news"
		echo "rsync success"
	else 
	  { date ; echo "$FROM $1 safecopy failed with $RSYNC_EXIT";
    } >> "${NEWSPAPER_DIRECTORY}/safecopy.news"
		echo "rsync failed with $RSYNC_EXIT"
		echo $RSYNC_OUT
	fi
}
ssh "$FROM" ./shadowCreate.sh
echo $DIRECTORIES
for i in "${DIRECTORIES[@]}"
do
	echo "rsyncing $i"
	rsync_folder "$i"
done
ssh "$FROM" ./shadowDelete.sh

#if [ -n $SHUTDOWN ]
#then
#ssh "$FROM"  shutdown /s /t 300
#fi
echo "done"

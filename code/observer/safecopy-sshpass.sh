#!/bin/bash
#    SafeCopy is a set of scripts for doing pull safecopys from windows
#    Copyright (C) 2016  LiberIT Liberty Information Technology Services
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -f|--from) #IP or resolvable hostname
    FROM="$2"
    shift # past argument
    ;;
    -t|--to) # destination directory
    TO="$2"
    shift # past argument
    ;;
    -d|--directories)
	echo $2
    eval "$2"
    shift # past argument
    ;;
    -n|--newspaper) #log
    NEWSPAPER_DIRECTORY="$2"
    shift # past argument
    ;;
    -p|--password) #log
    PASSWORD="$2"
    shift # past argument
    ;;
    -s|--shutdown) #log
    SHUTDOWN="true $2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

# establish defaults

eval "$RSYNC_EVAL"
if [ -z "${RSYNC_OPTS}" ];  # if rsync opts not defined set them
then 
  RSYNC_OPTS=(-ras --stats --delete --no-p --no-g --no-o --inplace -e "ssh -C")
fi
SUCCESS=0
function rsync_folder () {
  #shellcheck disable=SC2068
	sudo rsync ${RSYNC_OPTS[@]} --exclude="Application Data/Application Data" \
    --rsh="/usr/bin/sshpass -p $PASSWORD ssh -C -l safecopy" \
    "$FROM":shadow/"$1" "$TO"
  RSYNC_EXIT=$?
	#check if failed
	if [ "$RSYNC_EXIT" = "$SUCCESS" ] || [ "$RSYNC_EXIT" = 23 ]
	then
	  { date; echo "$FROM $1 safecopy success" ; 
    }  >> "${NEWSPAPER_DIRECTORY}/safecopy.news"
		echo "rsync success"
	else 
	  { date ; echo "$FROM $1 safecopy failed with $RSYNC_EXIT";
		echo "rsync failed with $RSYNC_EXIT"
    } >> "${NEWSPAPER_DIRECTORY}/safecopy.news"
	fi
}
echo "creating shadow"
sshpass -p "$PASSWORD" ssh "$FROM"  ./shadowCreate.sh
echo "rsyncing directories"
for i in "${DIRECTORIES[@]}"
do
	echo "rsyncing $i"
	rsync_folder "$i"
done
sshpass -p "$PASSWORD" ssh "$FROM"  ./shadowDelete.sh
if [ -n $SHUTDOWN ]
then
sshpass -p "$PASSWORD" ssh "$FROM"  shutdown /s /t 300
fi
echo "done"

#!/bin/bash
#    SafeCopy is a set of scripts for doing pull safecopys from windows
#    Copyright (C) 2016  LiberIT Liberty Information Technology Services
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMPANY="example"
EMAIL_RECIPIENTS="admin@example.com"
NAME="Someone Example"
DEVICES="/dev/sda2 /dev/sdb1"

SAFECOPY_DIRECTORY="/var/backups/safecopy/"

export NEWSPAPER_DIRECTORY="/tmp/"
export RSYNC_EVAL='RSYNC_OPTS=(-ravs --delete --no-p --no-g --no-o --inplace
        -e "ssh -C")'

DIR="${BASH_SOURCE%/*}"
echo "" > $NEWSPAPER_DIRECTORY/safecopy.news
#echo "Scanner"
"$DIR"/safecopy-pubkey.sh --from ServerAdmin@192.168.1.100 \
  --to $SAFECOPY_DIRECTORY/Scanner  \
  -d 'DIRECTORIES=("JOB FILES H&M" Users)' -p password
echo "Julie-PC"
"$DIR"/safecopy-sshpass.sh -f safecopy@192.168.1.101 -t $SAFECOPY_DIRECTORY/Julie-PC\
  -d 'DIRECTORIES=("JOB FILES H&M" Users)' -p password

#prepare emails
echo "email"
sudo btrfs filesystem defragment -c "$SAFECOPY_DIRECTORY"
DATE=$(date +%Y-%m-%d);
OUT=/tmp/safecopy-${COMPANY}-${DATE}.txt
"$DIR"/safecopy-mail.sh -t $EMAIL_RECIPIENTS  -n $NAME -d "$DEVICES"
#shellcheck disable=SC2068
#for i in ${EMAIL_RECIPIENTS[@]}
#do
#  mpack -s "safe copy" "$OUT" "$i"
#done

#!/bin/bash
#    SafeCopy is a set of scripts for doing pull safecopys from windows
#    Copyright (C) 2016  LiberIT Liberty Information Technology Services
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -t|--to) # destination addresses
    TO="$2"
    shift # past argument
    ;;
    -n|--name) #name
    NAME="$2"
    shift # past argument
    ;;
    -d|--devices) #important hard drive devices
    DEVICES="$2"
    shift # past argument
    ;;
    --newspaper) #log
    NEWSPAPER_DIRECTORY="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done


DATE=$(date +%Y-%m-%d);
if [ -z "${RSYNC_OPTS}" ]; 
then 
  NEWSPAPER_DIRECTORY="/tmp/"
fi
#NAME="location"
OUT=/tmp/$(echo "safecopy-${NAME}-${DATE}.txt" | sed -e 's/[^A-Za-z0-9._-]/_/g' )
STATUS="success"

echo "" > "$OUT"

function most_recent_modification_date () {
	FILE="$(find . -type f -printf '%T@ %p\n' | sort -rn | head -1 | cut -f2- -d" ")"
	echo "$FILE"
	stat -c %y "$FILE" |cut -d ' ' -f1 
	
}

{ 
 echo -n "# Safe Copy Summary from "; 
  cat /etc/hostname; 
#
echo "## Client Safe Copies"

if grep -qi failed < "$NEWSPAPER_DIRECTORY/safecopy.news" 
then 
  echo "safecopy failed"
  cat "$NEWSPAPER_DIRECTORY/safecopy.news"
  STATUS="failed"
elif grep -qi success < "$NEWSPAPER_DIRECTORY/safecopy.news" 
then
  echo "safecopy successful"
  cat "$NEWSPAPER_DIRECTORY/safecopy.news"
else 
  echo "safecopy failed"
  cat "$NEWSPAPER_DIRECTORY/safecopy.news"
  STATUS="failed"
fi
echo "## Server Info" 
for i in $DEVICES
do
	df -h $i
	sudo /usr/sbin/smartctl $i -H|grep ^SMART
done
#echo "#### snapshot list"
#snapper -c rsync-safecopy cleanup timeline
#snapper -c rsync-safecopy list
} >> "$OUT"

echo mailing

for i in ${TO}
do
  echo | mailx -s "$NAME safeCopy $STATUS" "$i" < $OUT
done

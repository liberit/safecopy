#!/bin/bash
#    SafeCopy is a set of scripts for doing pull safecopys from windows
#    Copyright (C) 2016  LiberIT Liberty Information Technology Services
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# wmic shadowcopy delete || exit 1 # warning: deletes all shadow copies
echo 'vssadmin delete shadows  /quiet /shadow='"$(cat shadow_id.txt)"|cmd
rm shadow
rm shadow_id.txt

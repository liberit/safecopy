#!/bin/bash
# for running on cygwin machines
#wmic shadowcopy delete
#wmic shadowcopy call create volume=C:\\
#ln -sf $(ls /proc/sys/Device/HarddiskVolumeShadowCopy* -1 | tail -n 1)/ /mnt/shadow

wmic shadowcopy call create volume=C:\\ | grep ShadowID |\
  sed -e 's/^.*=.//;s/;$//' > shadow_id.txt || exit 1
if [ -e ./shadow ]
then 
  rm ./shadow
fi
copy_number="$(find /proc/sys/Device/HarddiskVolumeShadowCopy[0-9]* | grep -o [0-9]* | sort -h | tail -n 1)/"
ln -sf "/proc/sys/Device/HarddiskVolumeShadowCopy$copy_number" \
  ./shadow || exit 1
exit 0

